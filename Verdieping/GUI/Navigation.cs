﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Verdieping.GUI;

namespace Verdieping
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            GraphsPrice Open = new GraphsPrice();
            Open.Show();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            Pricing Open1 = new Pricing();
            Open1.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            Medieval Open2 = new Medieval();
            Open2.Show();
        }
    }
}
